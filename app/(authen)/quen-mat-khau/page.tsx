

import ForgotContainer from "./components/ForgotContainer";

const ForgotPass = () => {
    return (
        <div className='flex-1'>
            <div className='container h-full'>
                <div className='w-full h-full flex justify-center items-center'>
                    <ForgotContainer />
                </div>
            </div>
        </div>
    );
};

export default ForgotPass;

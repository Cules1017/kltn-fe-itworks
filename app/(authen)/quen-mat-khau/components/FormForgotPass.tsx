"use client";
import { Button } from "@/app/components/ui/button";
import { Input } from "@/app/components/ui/input";
import { LockKeyhole } from "lucide-react";
import { ReloadIcon } from "@radix-ui/react-icons";
import { useContext, useState } from "react";
import Link from "next/link";
import { Form, Modal } from "antd";
import { ProfileContext } from "@/app/(protected)/profile/context/ProfileContext";
import InputOtp from "@/app/client/post/[id]/components/InputOtp";

interface IFormFogotPass {}

const FormFogotPass: React.FC<IFormFogotPass> = ({}) => {
  const [form] = Form.useForm();

  const { onOpenModal } = useContext(ProfileContext);

  const [verify, setVerify] = useState<boolean>(false);
  const [open, setOpen] = useState<boolean>(false);
  const showModal = () => {
    setOpen(true);
  };

  const hideModal = () => {
    setOpen(false);
  };
  const handleSubmitForm = async (values: any) => {
    // console.log('Received values of form: ', values);
    // const result = await loginServices.changePassword(values);
    // console.log(result);
    //show thông báo ở đây
  };
  return (
    <div className="px-12 py-4 mt-8 overflow-x-hidden max-w-[500px] mx-auto border border-solid border-[#d5e0d5] rounded-2xl">
      <div className="px-8">
        <h1 className="text-[28px] mt-6 mb-6 leading-8 text-center font-semibold">
          Vui lòng nhập mật khẩu mới
        </h1>

        <div>
          {/* <div className="relative border-2 border-solid border-[#e4ebe4] rounded-lg mb-6"> */}
            <Form
              form={form}
              name="dependencies"
              autoComplete="off"
              onFinish={handleSubmitForm}
              style={{width: '100%'}}
              // style={{ width:  }}
              layout="vertical"
            >
              <Form.Item
                label="Mật khẩu cũ"
                name="old_password"
                rules={[{ required: true }]}
              >
                <Input type="password" />
              </Form.Item>

              <Form.Item
                label="Mật khẩu mới"
                name="new_password"
                rules={[{ required: true }]}
              >
                <Input type="password" />
              </Form.Item>

              {/* Field */}
              <Form.Item
                label="Xác nhận mật khẩu"
                name="password2"
                dependencies={["new_password"]}
                rules={[
                  {
                    required: true,
                  },
                  ({ getFieldValue }) => ({
                    validator(_, value) {
                      if (!value || getFieldValue("new_password") === value) {
                        return Promise.resolve();
                      }
                      return Promise.reject(
                        new Error(
                          "Vui lòng kiểm tra lại mật khẩu mới và mật khẩu xác nhận phải giống nhau!!"
                        )
                      );
                    },
                  }),
                ]}
              >
                <Input type="password" />
              </Form.Item>
              
              {/* Render Props
      <Form.Item noStyle dependencies={['password2']}>
        {() => (
          <Typography>
            <p>
              Only Update when <code>password2</code> updated:
            </p>
            <pre>{JSON.stringify(form.getFieldsValue(), null, 2)}</pre>
          </Typography>
        )}
      </Form.Item> */}
            </Form>
          </div>

          <Button className="block w-full bg-[#108a00] hover:bg-[#108a00]/80  rounded-[10rem] ">
            {/* {loading && (
                            <ReloadIcon className='mr-2 h-4 w-4 animate-spin inline-flex' />
                        )} */}
            Xác thực
          </Button>
          <Modal
            className="text-center"
            title="Nhập mã OTP, mã OTP đã được gởi về mail của bạn"
            open={open}
            onCancel={hideModal}
            footer={[]}
          >
            <InputOtp setVerify={setVerify}></InputOtp>
          </Modal>
        </div>
      </div>
    // </div>
  );
};

export default FormFogotPass;

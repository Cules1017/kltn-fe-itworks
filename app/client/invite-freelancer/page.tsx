"use client";

import {
  SearchBarProvider,
} from "./context/SearchBarContext";

import Freelancers from "./components/Freelancers";
import Pagiantion from "./components/Pagination";

const ListFreelancer = () => {
  return (
    <>
     <h1 className="text-lg font-bold">Danh sách ứng viên đề xuất</h1>
    <SearchBarProvider>
      <div className="relative">
        {/* <SearchBar /> */}
        <Freelancers />
        <Pagiantion />
      </div>
    </SearchBarProvider>
    </>
   
  );
};

export default ListFreelancer;

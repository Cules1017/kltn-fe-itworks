"use client";

import { freelancerServices } from "@/app/services/freelancer.services";
import { AppliedJob } from "@/app/types/freelancer.type";
import { DetailClientPost } from "@/app/types/client.types";
import React, { useCallback, useContext, useEffect, useState } from "react";
import TaskBoardProvider from "./TaskBoard/TaskBoardContext";
import TaskBoard from "./TaskBoard";
import LoadingTaskPage from "../loading";
import { Button, Modal } from "antd";
import { smartContractService } from "@/app/services/thirbWeb.services";
import { useContract } from "@thirdweb-dev/react";
import { appConfig } from "@/app/configs/app.config";
import { ReloadIcon } from "@radix-ui/react-icons";
import { useRouter } from "next/navigation";
import { NotificationContext } from "@/app/providers/NotificationProvider";
import { useStateContext } from "@/context";
import { clientServices } from "@/app/services/client.services";

interface IManageTask {
  id: string;
}
const ManageTask: React.FC<IManageTask> = ({ id }) => {
  const [loading, setLoading] = useState(false);
  const [post, setPost] = useState<DetailClientPost | null>(null);
  const [jobs, setJobs] = useState<AppliedJob[]>([]);
  const [infoContract, setInfoContract] = useState<any>(null);
  const { openNotificationWithIcon } = useContext(NotificationContext);
  const [loadingConfirmCompleteJob, setLoadingConfirmCompleteJob] =
    useState(false);
  const [openModalReview, setOpenModalReview] = useState(false);
  const router = useRouter();
  const { address, connect } = useStateContext();
  // const { contract } = useContract(appConfig.contractId);
  const { contract } = useContract(appConfig.contractId);
  useEffect(() => {
    const fetchFreelancerJobs = async () => {
      const res = await freelancerServices.getAppliedJobs({});
      setJobs(res?.data?.data);
    };
    fetchFreelancerJobs();
  }, []);

  useEffect(() => {
    const getContractDetail = async () => {
      if (contract) {
        const data = await smartContractService.getDetailContractByJobId(
          parseInt(id),
          contract
        );
        setInfoContract(data);
        if (data?.status == 2) setOpenModalReview(true);
        if(data?.status === 3)  router.push(`/client/job/${id}/reviewing-job`);
      }
    };
    getContractDetail();
  }, [contract]);

  useEffect(() => {
    const fetchPostData = async (jobId: string) => {
      try {
        setLoading(true);
        const res = await freelancerServices.getPost(jobId);
        if (res.data) {
          setPost(res.data);
        }
      } catch (error) {
      } finally {
        setLoading(false);
      }
    };
    if (id) {
      fetchPostData(id);
    }
  }, [id]);

  const currentStatusJob = (jobs || []).find(
    (j) => j.job_id?.toString() === id
  );
  const PopupReview = () => {
    Modal.success({
      content: "some messages...some messages...",
    });
  };


  // Xử lý hoàn thành hợp đồng
  const handleCompletedJob = useCallback(() => {
    const checkData = async () => {
      try {
        setLoadingConfirmCompleteJob(true);
        const res = await clientServices.completeContract(
          parseInt(id)
        );
        if (res.result !=0) {
          openNotificationWithIcon(
            "error",
            `${res.message}`,
            "Xác nhận hoàn thành công việc thất bại."
          );
        }else{
          const data = await contract?.call("finalizeContract", [
            infoContract?.contract_id,
          ]);
          if (data != undefined) {
            openNotificationWithIcon(
              "success",
              "Thành công",
              "Xác nhận công việc thành công."
            );
            router.push(`/info-contract/${id}`);
          } else {
            openNotificationWithIcon(
              "error",
              "Thất bại",
              "Xác nhận công việc thành công thất bại"
            );
          }
        }
      
      } catch (error) {
        openNotificationWithIcon(
          "error",
          "Thất bại",
          "Xác nhận công việc thành công thất bại"
        );
        // setLoading(false);
      } finally {
        setLoadingConfirmCompleteJob(false);
      }
    };
    if (address) {
      checkData();
    } else {
      connect();
    }
  }, [contract, id]);

  return (
    <>
      {loading ? (
        <LoadingTaskPage />
      ) : (
        <div>
          {/* <div className="flex justify-between">
            <h2 className="text-4xl font-semibold -tracking-[1px]">
              Quản lý task - {post?.title}
            </h2>
          </div> */}
          <div className="flex justify-between">
            <div>
              <p className="text-3xl font-semibold -tracking-[1px]">
                Quản lý task - {post?.title}
              </p>
            </div>
            {/* <div className="">
              <Button
                type="primary"
                className="!bg-blue-500"
                size="large"
                onClick={handleCompletedJob}
              >
                {loadingConfirmCompleteJob && (
                  <ReloadIcon className="mr-2 h-4 w-4 animate-spin inline-flex" />
                )}
                {address
                  ? "Xác nhận hoàn thành"
                  : "Kết nối ví MetaMask để xác nhận hoàn thành"}
              </Button>
            </div> */}
            <div className="">
              {infoContract?.status === 2 ? (
                <Button
                  type="primary"
                  className="!bg-blue-500"
                  size="large"
                  onClick={handleCompletedJob}
                >
                  {loadingConfirmCompleteJob && (
                    <ReloadIcon className="mr-2 h-4 w-4 animate-spin inline-flex" />
                  )}
                  {address
                    ? "Xác nhận hoàn thành"
                    : "Kết nối ví MetaMask để xác nhận hoàn thành"}
                </Button>
              ) : infoContract?.status === 3 ? (
                <Button
                  type="primary"
                  className="!bg-green-500"
                  size="large"
                  //  onClick={handleCompletedJob}
                >
                  Đã hoàn thành
                </Button>
              ) : null}
            </div>
          </div>
          <div className="my-8">
            <TaskBoardProvider>
              <TaskBoard jobId={id} />
            </TaskBoardProvider>
          </div>
        </div>
      )}
      <Modal
        title="Modal 1000px width"
        centered
        open={openModalReview}
        onOk={() => setOpenModalReview(false)}
        onCancel={() => setOpenModalReview(false)}
        width={700}
      >
        {" "}
        <div
          style={{ padding: 20 }}
          dangerouslySetInnerHTML={{ __html: post?.desc || "" }}
        />
        <strong>
          Đã được freelancer báo cáo hoàn thành, bạn hãy review lại và xác nhận
          lần nữa
        </strong>
      </Modal>
    </>
  );
};

export default ManageTask;

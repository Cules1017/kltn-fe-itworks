'use client';

import {
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from '@/app/components/ui/table';
import { appConfig } from '@/app/configs/app.config';
import {
  IContractList,
  smartContractService,
} from '@/app/services/thirbWeb.services';
import { ContractStatus } from '@/app/types/contract.type';
import { useContract } from '@thirdweb-dev/react';
import React, { useContext, useEffect, useState } from 'react';
import Cookies from 'js-cookie';
import { AuthContext } from '@/app/providers/AuthProvider';
import { Pagination, Spin } from 'antd';
import { useRouter, useSearchParams } from 'next/navigation';

const PAGE_SIZE = 5;

function Contracts() {
  const router = useRouter();
  const searchParams = useSearchParams();
  const account_type = Cookies.get('account_type');
  const { contract } = useContract(appConfig.contractId);
  const { user } = useContext(AuthContext);
  const [contracts, setContracts] = useState<IContractList[]>([]);
  const [isLoading, setIsLoading] = useState(false);

  // Pagination 0x00ad418879Ea1Ea26ADb2e933F6Db270D92b567B 
  const [currentPage, setCurrentPage] = useState(1);
  const [total, setTotal] = useState(70);

  const getContractStatus = (status: ContractStatus) => {
    switch (status) {
      case ContractStatus.Created:
        return 'Đã tạo';
      case ContractStatus.FreelancerSigned:
        return 'Freelancer đã ký';
      case ContractStatus.FreelancerCompleted:
        return 'Freelancer đã hoàn thành';
      case ContractStatus.ClientComfirm:
        return 'Hợp đồng kết thúc';
      case ContractStatus.FreelancerCancel:
        return 'Đã hủy do client';
      case ContractStatus.ClientCancel:
        return 'Đã hủy do freelancer';
    }
  };

  useEffect(() => {
    getContracts();
  }, [currentPage, user, contract]);

  const getContracts = async () => {
    if (account_type == 'freelancer') getFreelancersContract();
  };

  const getFreelancersContract = async () => {
    console.log(user, contract, appConfig.contractId);
    if (!isLoading && user && contract) {
      setIsLoading(true);
      try {
        const res = await smartContractService.getContractsByFreelancerId(
          user!.id,
          contract
        );

        if (res) {
          console.log(searchParams.get('completed'));

          if (!searchParams.get('completed')) setContracts(res);
          else
            setContracts(
              res.filter((x) => x.status == ContractStatus.ClientComfirm)
            );
          setTotal(res.length);
        }
      } catch (error) {
      } finally {
        setIsLoading(false);
      }
    }
  };

  const onPageChange = (page: number, pageSize: number) => {
    setCurrentPage(page);
  };

  const directToDetail = (contract: IContractList) => {
    if (
      contract.status != ContractStatus.ClientCancel &&
      contract.status != ContractStatus.FreelancerCancel
    ) {
      router.push('/info-contract/' + contract.jobIdcurent);
    }
  };

  return (
    <div>
      <div className='flex items-center justify-center mb-5'>
        <h2 className='text-4xl leading-[48px] tracking-normal font-medium mt-1 mb-2'>
          Hợp đồng
        </h2>
      </div>
      <table className='overscroll-none mx-auto'>
        <TableHeader>
          <TableRow>
            <TableHead className='w-[100px] text-center font-medium'>
              STT
            </TableHead>
            <TableHead>Tiêu đề</TableHead>
            <TableHead className='text-center'>Trạng thái</TableHead>
          </TableRow>
        </TableHeader>
        <TableBody className='text-[14px]'>
          {contracts.map((contract, index) => (
            <TableRow
              key={`applied-job-item-${index}`}
              onClick={() => {
                directToDetail(contract);
              }}
            >
              <TableCell className='font-medium text-center'>
                {index + 1}
              </TableCell>
              <TableCell>{contract.title}</TableCell>
              <TableCell>{getContractStatus(contract.status)}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </table>

      {total != 0 && (
        <div className='flex justify-center pt-5'>
          <Pagination
            defaultCurrent={currentPage}
            total={total}
            pageSize={PAGE_SIZE}
            onChange={onPageChange}
          />
        </div>
      )}
      {isLoading && <Spin fullscreen />}
    </div>
  );
}

export default Contracts;

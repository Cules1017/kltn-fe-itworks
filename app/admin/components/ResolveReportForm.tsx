import { Textarea } from '@/app/components/ui/textarea';
import { NotificationContext } from '@/app/providers/NotificationProvider';
import { ReportList, ReportStatus } from '@/app/types/reports.type';
import { Button, Modal, Select } from 'antd';
import { ChangeEvent, useContext, useEffect, useState } from 'react';

interface ResolveReportFormProps {
  isOpen: boolean;
  onCancel: () => void;
  onResolve: (status: ReportStatus, result: string) => void;
  report: ReportList;
}

function ResolveReportForm({
  isOpen,
  onCancel,
  report: data,
  onResolve,
}: ResolveReportFormProps) {
  const [report, setReport] = useState<ReportList>(data);
  const [isModalConfirmOpen, setIsModalConfirmOpen] = useState(false);
  const [isModalCancelOpen, setIsModalCancelOpen] = useState(false);

  // Resolve
  const [resultText, setResultText] = useState(report.results ?? '');
  const [selectedStatus, setSelectedStatus] = useState<ReportStatus>(0);

  const { openNotificationWithIcon } = useContext(NotificationContext);

  const handleChangeSelectStatus = (value: string) => {
    setSelectedStatus(Number(value));
  };

  useEffect(() => {
    setReport(data);
    setResultText(data.results ?? '');
    console.log(report.content_file?.length);
  }, [data]);

  function downloadFile(url: string): void {
    const anchorElement = document.createElement('a');
    anchorElement.href = url;
    anchorElement.target = '_blank';
    anchorElement.download = getFilenameFromUrl(url); // This can specify a filename for downloading
    document.body.appendChild(anchorElement); // Append to body to ensure it's in the DOM
    anchorElement.click(); // Simulate click
    document.body.removeChild(anchorElement); // Clean up the DOM after triggering download
  }

  function getFilenameFromUrl(url: string): string {
    try {
      const urlObj = new URL(url);
      const pathname = urlObj.pathname;
      const lastSlashIndex = pathname.lastIndexOf('/');
      const filename = pathname.substring(lastSlashIndex + 1);
      return filename; // Assume no query or hash needed for filenames
    } catch (error) {
      console.error('Invalid URL:', error);
      return '';
    }
  }

  return (
    <div>
      {isOpen && (
        <div>
          <Modal
            title=''
            open={isOpen}
            onOk={() => {}}
            onCancel={() => {
              onCancel();
            }}
            width={800}
            closable={false}
            footer={(_, { OkBtn, CancelBtn }) => (
              <div>
                <h2 className='text-center mb-3 text-[23px] font-bold'>
                  Xử lý báo cáo
                </h2>
                <div className='grid grid-cols-5 h-[400px] mt-3  max-h-full'>
                  <div className='col-span-3 pr-6 flex flex-col max-h-[400px]'>
                    <Textarea
                      readOnly
                      className='flex-1 p-4 text-start resize-none'
                      value={report.content}
                    />
                    <Button
                      className='mt-5'
                      onClick={() => {
                        downloadFile(report.content_file);
                      }}
                      disabled={
                        !report.content_file || report.content_file.length === 0
                      }
                    >
                      Xem nội dung
                    </Button>
                  </div>
                  <div className='col-span-2 flex flex-col'>
                    <Textarea
                      readOnly={report.results?.length != 0}
                      placeholder='Nội dung xử lý'
                      className='flex-1 resize-none'
                      value={resultText}
                      onChange={(e: ChangeEvent<HTMLTextAreaElement>) => {
                        setResultText(e.target.value);
                      }}
                    />
                    <div className='p-3 flex justify-center items-center'>
                      <span className='text-[14px] p-2 pr-8 font-bold'>
                        Xử lý
                      </span>
                      <Select
                        disabled={report.results?.length != 0}
                        className='text-center'
                        defaultValue='0'
                        style={{ width: 140 }}
                        onChange={handleChangeSelectStatus}
                        options={[
                          { value: '0', label: 'Loại bỏ' },
                          { value: '1', label: 'Chấp nhận' },
                        ]}
                      />
                    </div>

                    <div className='flex'>
                      <Button
                        disabled={report.results?.length != 0}
                        className='mr-3 flex-1'
                        onClick={() => {
                          setIsModalConfirmOpen(true);
                        }}
                      >
                        Hoàn tất
                      </Button>
                      <Button
                        disabled={report.results?.length != 0}
                        className='flex-1'
                        onClick={() => {
                          setIsModalCancelOpen(true);
                        }}
                      >
                        Hủy
                      </Button>
                    </div>
                  </div>
                </div>

                <Modal
                  title='Cảnh báo'
                  open={isModalConfirmOpen}
                  onOk={() => {}}
                  onCancel={() => {
                    setIsModalConfirmOpen(false);
                  }}
                  closable={false}
                  footer={(_, { OkBtn, CancelBtn }) => (
                    <>
                      <CancelBtn />
                      <Button
                        className='text-[#4096ff] border-[#4096ff] hover:text-[#fff] hover:bg-[#4096ff]'
                        onClick={async () => {
                          // handleDeleleUser();
                          setIsModalConfirmOpen(false);

                          if (resultText.length == 0) {
                            openNotificationWithIcon(
                              'error',
                              'Lỗi',
                              'Nội dung xử lý là bắt buộc'
                            );
                          } else {
                            await onResolve(selectedStatus, resultText);
                            onCancel();
                            // setTimeout(() => {
                            //   setTimeout(() => {
                            //   }, 100);
                            // }, 100);
                          }
                        }}
                      >
                        Oke
                      </Button>
                    </>
                  )}
                >
                  <p>Bạn có muốn hoàn tất xử lý ?</p>
                </Modal>

                <Modal
                  title='Cảnh báo'
                  open={isModalCancelOpen}
                  onOk={() => {}}
                  onCancel={() => {
                    setIsModalCancelOpen(false);
                  }}
                  closable={false}
                  footer={(_, { OkBtn, CancelBtn }) => (
                    <>
                      <CancelBtn />
                      <Button
                        className='text-[#4096ff] border-[#4096ff] hover:text-[#fff] hover:bg-[#4096ff]'
                        onClick={() => {
                          setIsModalCancelOpen(false);
                          setTimeout(() => {
                            onCancel();
                          }, 100);
                        }}
                      >
                        Oke
                      </Button>
                    </>
                  )}
                >
                  <p>Bạn có muốn hủy ?</p>
                </Modal>
              </div>
            )}
          ></Modal>
        </div>
      )}
    </div>
  );
}

export default ResolveReportForm;

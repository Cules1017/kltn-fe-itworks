interface IAppConfig {
    apiUrl: string,
    contractId: string,
    mceKey: string,
    keyHash: string,
    aws3:string
}

export const appConfig: IAppConfig = {
    apiUrl: process.env.NEXT_PUBLIC_APP_API_URL || '',
    contractId:process.env.NEXT_PUBLIC_APP_ID_CONTRACT||'',
    mceKey: process.env.NEXT_PUBLIC_APP_MCE_KEY || '',
    keyHash: process.env.NEXT_PUBLIC_KEY_HASH || '',
    aws3: process.env.NEXT_PUBLIC_AWS3_DOMAN||'https://my-final.s3.ap-southeast-1.amazonaws.com/my-final/'
}  
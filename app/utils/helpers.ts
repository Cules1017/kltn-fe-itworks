import { notification } from "antd";
import { commonServices } from "../services/common.services";
import { NotificationType } from "../types/common.types";
import { appConfig } from "../configs/app.config";
// const [api, contextHolder] = notification.useNotification();

export const sendNotification = async (data: any) => {
  try {
    // setIsGettingPosts(true);
    const res = await commonServices.sendNotication(data);
    if (res.status === 200) {
      console.log("send notification success", res);
    }
  } catch (error) {
    console.log("error", error);
  } finally {
    // setIsGettingPosts(false);
  }
};
function base64ToBlob(base64: any, mime: any) {
  // Loại bỏ tiền tố 'data:[<mediatype>][;base64],'
  const base64Data = base64.split(',')[1] || base64;
  const byteCharacters = atob(base64Data);
  const byteNumbers = new Array(byteCharacters.length);
  for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
  }
  const byteArray = new Uint8Array(byteNumbers);
  return new Blob([byteArray], {type: mime});
}
export const uploadBase64Image = async (base64String: any, pathUrl: any) => {
  const mimeType = 'image/png';
  // Chuyển đổi Base64 thành Blob
  const blob = base64ToBlob(base64String, mimeType);

  // Tạo một đối tượng FormData
  const formData = new FormData();
  formData.append('sign', blob, 'uploaded_image.png');  // Thay 'uploaded_image.png' bằng tên file mong muốn

  try {
      const response = await fetch(appConfig.apiUrl+pathUrl, {
          method: 'POST',
          body: formData
      });

    if (response.ok) {
        console.log(response);
        
          const result = response.text();
        return result;
      } else {
          return false
      }
  } catch (error) {
      return false
  }
}


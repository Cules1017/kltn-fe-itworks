
const constants = {
    // address meta mask
    ADDRESS_META_MASK: '0x12C33E9f080907dfF4BFaE4A841f4F2cA7E975eD',
    //Configs phaan trang
    PAGE_SIZE: 30,


    ACCEPT_INVITE: 1,
    REJECT_INVITE: -1,
    PENDING_INVITE: 0,
}
export default constants;